%.py.tex:	%.py
	./code2tex.fish py <$^ >$@
%.cc.tex:	%.cc
	indent -kr -brf <$^ | ./code2tex.fish c++ >$@
%.pdf:	%.tex
	pdflatex --interaction=batchmode $^
	$(RM) $*.aux $*.log
%.ppm:	%.pdf
	pdftoppm -singlefile -r 600 <$^ | pnmcrop >$@
%.png:	%.ppm
	pnmtopng <$^ >$@
