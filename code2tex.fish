#!/usr/bin/fish
if [ $argv[1] = 'py' ]
	set lang Python
else if [ $argv[1] = 'c++' ]
	set lang c++
else
	echo "Unknown language type $argv[1]"
	exit 1
end

echo "\
\documentclass{article}

\usepackage{graphics}
\usepackage{listings}
\usepackage[dvipsname]{xcolor}
\usepackage{pifont}

\pagenumbering{gobble}

\lstset{frame=none,
	language={$lang},
	aboveskip=2mm,
	belowskip=2mm,
	showstringspaces=false,
	columns=flexible,
	basicstyle=\ttfamily,
	numbers=none,
	keywordstyle=\color{blue},
	commentstyle=\color{gray},
	stringstyle=\color{strdark},
	breaklines=true,
	breakatwhitespace=true,
	tabsize=4,
	escapechar=`
}

\definecolor{strdark}{cmyk}{1, 1, 0, 0}
\definecolor{darkgreen}{cmyk}{1, 0.2, 0.8, 0.15}

\begin{document}

\begin{lstlisting}"

function colour_box -a num colour
	set ding expr 171 + $num
	sed -E "s/@@@{$num}@@@/\\fcolorbox{$colour}{$colour!10}{\\color{$colour!50!gray!50}~\\hspace{4mm}\\ding{$ding}\\hspace{4mm}~}/g"
end

colour_box 1 blue | colour_box 2 red | colour_box 3 aquamarine | colour_box 4 peach

echo "\end{lstlisting}"
echo "\end{document}"
