# code2png

This project aims to automate pretty-printing (with syntax-colouring) code files,
particularly Python files, into .png files, which are suitable for loading into
an LMS (like Blackboard or D2L), to slow down cheaters on quizzes.

It allows highlighted empty boxes to be included, via @@@1@@@ or @@@2@@@ (the
numbers in between the @ triplets influence the colour used).

# Usage

## Dependencies

You must have the following installed:

* GNU make
* A sensible LaTeX distribution (probably TeXLive), including the listings package
* NetPBM
* fish

## Example usage

Given a `foo.py` file, run: `make foo.py.png`
